/*
 * gsstreamingpad.c
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#include <string.h>

#include "gsstreamingpad.h"
#include "gdef.h"

#if 0
G_DEFINE_TYPE (GsStreamingPad, gs_streamingpad, G_TYPE_OBJECT)
#else
static void     gs_streamingpad_init              (GsStreamingPad        *self);
static void     gs_streamingpad_class_init        (GsStreamingPadClass *klass);
static gpointer gs_streamingpad_parent_class = ((void *)0);
static void     gs_streamingpad_class_intern_init (gpointer klass)
{
  gs_streamingpad_parent_class = g_type_class_peek_parent (klass);
  gs_streamingpad_class_init ((GsStreamingPadClass*) klass);
}

GType
gs_streamingpad_get_type (void)
{
  static volatile gsize g_define_type_id__volatile = 0;
  if (g_once_init_enter (&g_define_type_id__volatile))
    {
      GType g_define_type_id =
        g_type_register_static_simple (G_TYPE_OBJECT,
                                       g_intern_static_string ("GsStreamingPad-dvb"),
                                       sizeof (GsStreamingPadClass),
                                       (GClassInitFunc) gs_streamingpad_class_intern_init,
                                       sizeof (GsStreamingPad),
                                       (GInstanceInitFunc) gs_streamingpad_init,
                                       (GTypeFlags) 0);
      { {{};} }
      g_once_init_leave (&g_define_type_id__volatile, g_define_type_id);
    }
  return g_define_type_id__volatile;
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////

static void gs_streamingpad_dispose(GObject *object);
static void gs_streamingpad_finalize(GObject *object);


#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))

/*
 * X ==> [0, 1]
 * Y = (sin((pi * x) - pi) + 1) / 2 * 1000000
 */
#if 1 //20
static int spring_factor_table[] = {
		0, 6155, 24471, 54496, 95491, 146446, 206107, 273004, 345491, 421782,
		499999, 578217, 654508, 726995, 793892, 853553, 904508, 945503, 975528, 993844
};

#elif 1// 50
static int spring_factor_table[] = {
		0, 986, 3942, 8856, 15708, 24471, 35111, 47586, 61846, 77836,
		95491, 114743, 135515, 157726, 181288, 206107, 232086, 259123, 287110, 315937,
		345491, 375655, 406309, 437333, 468604, 500000, 531395, 562666, 593690, 624344,
		654508, 684062, 712889, 740876, 767913, 793892, 818711, 842273, 864484, 885256,
		904508, 922163, 938153, 952413, 964888, 975528, 984291, 991143, 996057, 999013
};

#elif 0 //100
static int spring_factor_table[] = {
		0, 246, 986, 2219, 3942, 6155, 8856, 12041, 15708, 19853,
		24471, 29559, 35111, 41122, 47586, 54496, 61846, 69628, 77836, 86459,
		95491, 104922, 114743, 124944, 135515, 146446, 157726, 169344, 181288, 193546,
		206107, 218958, 232086, 245479, 259123, 273004, 287110, 301426, 315937, 330631,
		345491, 360504, 375655, 390928, 406309, 421782, 437333, 452945, 468604, 484294,
		500000, 515705, 531395, 547054, 562666, 578217, 593690, 609071, 624344, 639495,
		654508, 669368, 684062, 698573, 712889, 726995, 740876, 754520, 767913, 781041,
		793892, 806453, 818711, 830655, 842273, 853553, 864484, 875055, 885256, 895077,
		904508, 913540, 922163, 930371, 938153, 945503, 952413, 958877, 964888, 970440,
		975528, 980146, 984291, 987958, 991143, 993844, 996057, 997780, 999013, 999753
};

#elif 0 // 10
static int spring_factor_table[] = {
		0, 24471, 95491, 206107, 345491, 500000, 654508, 793892, 904508, 975528, 1000000
};
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////


static void
gs_streamingpad_class_init (GsStreamingPadClass *klass){
    GObjectClass *base_class = G_OBJECT_CLASS (klass);

    base_class->dispose      = gs_streamingpad_dispose;
    base_class->finalize     = gs_streamingpad_finalize;

}

static void
gs_streamingpad_init (GsStreamingPad *spad){
    spad->mutex = g_mutex_new();
    spad->queue = g_queue_new();
    spad->name  = strdup("null");
    spad->cached_full_warning = TRUE;
    spad->delay_size = 0;
    spad->delay_spring_fator_x = 1;
    spad->priv = NULL;
    spad->caps = NULL;
    spad->format = GST_FORMAT_BYTES;
    spad->cached_size = 0;
    spad->max_cache_size = 16 * 0x100000;
    spad->iexception_count = 0;
    spad->oexception_count = 0;

    spad->start_time = GST_CLOCK_TIME_NONE;
    spad->base_time  = GST_CLOCK_TIME_NONE;
    spad->ilast_time = GST_CLOCK_TIME_NONE;
    spad->olast_time = GST_CLOCK_TIME_NONE;
    spad->icheck_time = GST_CLOCK_TIME_NONE;
    spad->ocheck_time = GST_CLOCK_TIME_NONE;
}

static
void gs_streamingpad_finalize(GObject *object)
{
    GsStreamingPad *spad = GS_STREAMINGPAD(object);

    while(g_queue_is_empty(spad->queue) == FALSE){
        GstBuffer *buffer = g_queue_pop_head(spad->queue);
        gst_buffer_unref(buffer);
    }

    g_mutex_free(spad->mutex);
    g_queue_free(spad->queue);
    SAFE_FREE(spad->name);
    if(spad->caps){
        gst_caps_unref(spad->caps);
        spad->caps = NULL;
    }

    G_OBJECT_CLASS(gs_streamingpad_parent_class)->finalize(object);
}

static
void gs_streamingpad_dispose(GObject *object)
{
    GsStreamingPad *spad = GS_STREAMINGPAD(object);

    G_OBJECT_CLASS(gs_streamingpad_parent_class)->dispose(object);
}

GsStreamingPad *
gs_streamingpad_new (const gchar* name)
{
    GsStreamingPad *pad = g_object_new(GS_TYPE_STREAMINGPAD, NULL);
    if(name){
        SAFE_FREE(pad->name);
        pad->name = strdup(name);
    }

    return pad;
}

void
gs_streamingpad_push_buffer(GsStreamingPad *spad, GstBuffer *buffer)
{
    g_mutex_lock(spad->mutex);
    {
        gst_buffer_ref(buffer);
        if(spad->start_time == GST_CLOCK_TIME_NONE){
            spad->start_time = buffer->timestamp;
        }

        g_queue_push_tail(spad->queue, buffer);
        spad->cached_size += buffer->size;

        int *size;
        if(spad->format == GST_FORMAT_BYTES){
            size = &spad->cached_size;
        }else if(spad->format == GST_FORMAT_BUFFERS){
            size = &spad->queue->length;
        }else{
            //do nothing
        }

        while(*size > spad->max_cache_size){
            GstBuffer * buf = gs_streamingpad_pop_buffer_unlock(spad);
            gst_buffer_unref(buf);

            if(spad->cached_full_warning){
                GST_WARNING("[%s], drop buffer pts=%lld !!", spad->name, buf->timestamp);
            }
        }

        spad->per_size = spad->cached_size / spad->queue->length;

        spad->ilast_time = buffer->timestamp;
    }
    g_mutex_unlock(spad->mutex);
}

GstBuffer *
gs_streamingpad_pop_buffer(GsStreamingPad* spad)
{
    GstBuffer *buffer;

    g_mutex_lock(spad->mutex);
    {
        buffer = gs_streamingpad_pop_buffer_unlock(spad);

        spad->olast_time = buffer->timestamp;
    }
    g_mutex_unlock(spad->mutex);

    return buffer;
}

GstBuffer *
gs_streamingpad_peek_head_buffer(GsStreamingPad* spad)
{
    GstBuffer *buffer;

    g_mutex_lock(spad->mutex);
    {
        buffer = g_queue_peek_head(spad->queue);
    }
    g_mutex_unlock(spad->mutex);

    return buffer;
}

GstBuffer *
gs_streamingpad_pop_buffer_unlock(GsStreamingPad* spad)
{
    GstBuffer *buffer;

    buffer = g_queue_pop_head(spad->queue);
    spad->cached_size -= buffer->size;

    return buffer;
}

void
gs_streamingpad_set_max_cache_size(GsStreamingPad* spad, int size)
{
    g_mutex_lock(spad->mutex);
    {
        spad->max_cache_size = size;
    }
    g_mutex_unlock(spad->mutex);
}

void
gs_streamingpad_set_cache_warning(GsStreamingPad* spad, gboolean on)
{
    g_mutex_lock(spad->mutex);
    {
        spad->cached_full_warning = on;
    }
    g_mutex_unlock(spad->mutex);
}

void
gs_streamingpad_set_delay_size(GsStreamingPad* spad, gint64 size)
{

    if (size < 0){
        av_printl(" *** WARNING: MUST size[%lld] >= 0", size);
        return ;
    }
    if(size == spad->delay_size) return;

//    av_printl("  @@@ %s %s delay = %lld", spad->name, gst_format_get_name(spad->format), size);

    g_mutex_lock(spad->mutex);
    {
        spad->delay_size = size;
    }
    g_mutex_unlock(spad->mutex);
}

gint64
gs_streamingpad_get_delay_size(GsStreamingPad* spad)
{
	gint64 size = 0;

    g_mutex_lock(spad->mutex);
    {
        size =  spad->delay_size;
    }
    g_mutex_unlock(spad->mutex);

    return size;
}


void
gs_streamingpad_set_spring_delay_size(GsStreamingPad* spad, gint64 size)
{
//    av_printl("  @@@ %s %s delay = %lld", spad->name, gst_format_get_name(spad->format), size);
    if (size < 0){
        av_printl(" *** WARNING: MUST size[%lld] >= 0", size);
        return ;
    }

    do{
    	// The ">=" means weighting for increase 'delay_size'
		if (size >= spad->delay_size){
			if (spad->delay_spring_fator_x < 0){
				spad->delay_spring_fator_x = 0;
			}else{
				spad->delay_spring_fator_x ++;

				if(spad->delay_spring_fator_x >= ARRAY_SIZE(spring_factor_table)){
					spad->delay_spring_fator_x = ARRAY_SIZE(spring_factor_table) - 1;
				}
			}

//			gint64 sz = ((size - spad->delay_size) * spring_factor_table[spad->delay_spring_fator_x] + 500000)/ 1000000 + spad->delay_size;
			gint64 sz = MXROUND((size - spad->delay_size) * spring_factor_table[spad->delay_spring_fator_x], 1000000) + spad->delay_size;

			av_printl("##Increase %s new_delay_size=%lld old_delay_size=%lld  sz=%lld",  spad->name, size, spad->delay_size, sz);

			gs_streamingpad_set_delay_size(spad, sz);
		}else if(size < spad->delay_size){
			if (spad->delay_spring_fator_x > 0){
				spad->delay_spring_fator_x = 0;
			}else{
				spad->delay_spring_fator_x --;
				if(ABS(spad->delay_spring_fator_x) >= ARRAY_SIZE(spring_factor_table)){
					spad->delay_spring_fator_x = -(ARRAY_SIZE(spring_factor_table) - 1);
				}
			}



//			gint64 sz = spad->delay_size - ((spad->delay_size - size) * spring_factor_table[ABS(spad->delay_spring_fator_x)] + 500000) / 1000000;
			gint64 sz = spad->delay_size - MXROUND((spad->delay_size - size) * spring_factor_table[ABS(spad->delay_spring_fator_x)], 1000000);


			av_printl("##Decrease %s new_delay_size=%lld old_delay_size=%lld  sz=%lld", spad->name, size, spad->delay_size, sz);

			gs_streamingpad_set_delay_size(spad, sz);
		}else{
			// Do Nothing!!
		}
    }while(0);


}

void
gs_streamingpad_set_delay_time(GsStreamingPad* spad, GstClockTime delay)
{
    if(spad->format == GST_FORMAT_BUFFERS){
        if(spad->major_type == STREAM_MAJOR_VIDEO){
            gint64 duration = GST_SECOND * spad->video.fps_den / spad->video.fps_num;
            gs_streamingpad_set_delay_size(spad, delay / duration) ;
        }else if(spad->major_type == STREAM_MAJOR_AUDIO){
            gint64 valid_size = (delay * spad->audio.rate * spad->audio.channels * spad->audio.width / 8) / GST_SECOND;
            gs_streamingpad_set_delay_size(spad, valid_size / spad->per_size) ;
        }else{
            //FIXME
        }

    }else if(spad->format == GST_FORMAT_BYTES){
        if(spad->major_type == STREAM_MAJOR_VIDEO){
            //FIXME
        }else if(spad->major_type == STREAM_MAJOR_AUDIO){
            //FIXME
        }else{
            //FIXME
        }
    }
}

int
gs_streamingpad_get_valid_size(GsStreamingPad* spad)
{
    int size = 0;

    if(spad->format == GST_FORMAT_BUFFERS){
        if(spad->delay_size >= spad->queue->length){
            size = 0;
        }else{
            size = spad->queue->length - spad->delay_size;
        }
    }else if(spad->format == GST_FORMAT_BYTES){
        if(spad->delay_size >= spad->cached_size){
            size = 0;
        }else{
            size = spad->cached_size - spad->delay_size;
        }
    }

    return size;
}

gboolean
gs_streamingpad_in_is_living(GsStreamingPad* spad)
{
    gboolean ret;
    g_mutex_lock(spad->mutex);
    {
        if(spad->ilast_time == GST_CLOCK_TIME_NONE){
            ret = FALSE;
        }else{
            if(spad->ilast_time != spad->icheck_time){
                spad->icheck_time = spad->ilast_time;
                ret = TRUE;
            }else{
                ret = FALSE;
            }
        }

        if(ret == TRUE){
            spad->iexception_count = 0;
        }else{
            spad->iexception_count ++;
        }
    }
    g_mutex_unlock(spad->mutex);

    return ret;
}

gboolean
gs_streamingpad_out_is_living(GsStreamingPad* spad)
{
    gboolean ret;
    g_mutex_lock(spad->mutex);
    {
        if(spad->olast_time == GST_CLOCK_TIME_NONE){
            ret = FALSE;
        }else{
            if(spad->olast_time != spad->ocheck_time){
                spad->ocheck_time = spad->olast_time;
                ret = TRUE;
            }else{
                ret = FALSE;
            }
        }

        if(ret == TRUE){
            spad->oexception_count = 0;
        }else{
            spad->oexception_count ++;
        }
    }
    g_mutex_unlock(spad->mutex);

    return ret;
}

gboolean
gs_streamingpad_set_caps(GsStreamingPad* spad, GstCaps *caps)
{
    gboolean ret = TRUE;
    g_mutex_lock(spad->mutex);
    {
        GstStructure *s;
        s = gst_caps_get_structure(caps, 0);

        av_printl("    ########## caps=%s", gst_caps_to_string(caps));

        gchar *name = gst_structure_get_name(s);
        if(g_str_has_prefix(name, "video/")){
            spad->major_type = STREAM_MAJOR_VIDEO;
            gst_structure_get_int(s, "width", &spad->video.width);
            gst_structure_get_int(s, "height", &spad->video.height);
            gst_structure_get_fraction(s, "framerate", &spad->video.fps_num, &spad->video.fps_den);

            av_printl("spad->video.fps_num=%d, spad->video.fps_den=%d", spad->video.fps_num, spad->video.fps_den);
        }else if(g_str_has_prefix(name, "audio/")){
            spad->major_type = STREAM_MAJOR_AUDIO;
            gst_structure_get_int(s, "channels", &spad->audio.channels);
            gst_structure_get_int(s, "rate",  &spad->audio.rate);
            gst_structure_get_int(s, "width", &spad->audio.width);
        }else if(g_str_has_prefix(name, "private/x-dvbsub")){
            spad->major_type = STREAM_MAJOR_SUBTITLE;
        }else{
            spad->major_type = STREAM_MAJOR_UNKNOWN;
        }

        spad->caps = caps;
    }
    g_mutex_unlock(spad->mutex);

    return ret;
}


