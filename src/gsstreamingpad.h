/*
 *  gsstreamingpad.h
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#ifndef _GS_STREAMINGPAD_H_
#define _GS_STREAMINGPAD_H_

#include <gst/gst.h>
#include <glib-object.h>

typedef enum _StreamMajorType {
    STREAM_MAJOR_UNKNOWN,
    STREAM_MAJOR_VIDEO,
    STREAM_MAJOR_AUDIO,
    STREAM_MAJOR_SUBTITLE
}StreamMajorType;

#define gs_streamingpad_ref(x)   (g_object_ref((x)))
#define gs_streamingpad_unref(x) (g_object_unref((x)))

typedef struct _GsStreamingPad          GsStreamingPad;
typedef struct _GsStreamingPadClass     GsStreamingPadClass;

struct _GsStreamingPad {
    GObject parent;

    gchar* name; //If this is a app-sink/src pad, the name same with them.

    GQueue *queue;
    GMutex *mutex;
    GstCaps *caps;
    GstFormat format;
    int cached_size;    //Has cached the bytes(TS)
    int max_cache_size;      //cached the MAX bytes(TS)
    gint64 delay_size;
    int delay_spring_fator_x;	 //Range [-100, 100]
    gboolean cached_full_warning;
    guint per_size;        //the size of all buffers' average in queue
    guint iexception_count, oexception_count; //if the stream can not stream data, this will be inc

    StreamMajorType major_type;

    union {
        struct{
            int width, height;
            int fps_num, fps_den;
        }video;
        struct{
            int rate;
            int width;
            int channels;
        }audio;
    };

    GstClockTime start_time;
    GstClockTime base_time;
    GstClockTime ilast_time, icheck_time;
    GstClockTime olast_time, ocheck_time;

    void  *priv;
};

struct _GsStreamingPadClass {
    GObjectClass parent_class;
};

#define GS_TYPE_STREAMINGPAD               (gs_streamingpad_get_type ())
#define GS_STREAMINGPAD(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), GS_TYPE_STREAMINGPAD, GsStreamingPad))
#define GS_IS_STREAMINGPAD(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), GS_TYPE_STREAMINGPAD))
#define GS_STREAMINGPAD_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), GS_TYPE_STREAMINGPAD, GsStreamingPadClass))
#define GS_IS_STREAMINGPAD_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), GS_TYPE_STREAMINGPAD))
#define GS_STREAMINGPAD_GET_CLASS(object)  (G_TYPE_INSTANCE_GET_CLASS ((object), GS_TYPE_STREAMINGPAD, GsStreamingPadClass))


GType gs_streamingpad_get_type (void);


GsStreamingPad *gs_streamingpad_new (const gchar* name);
void        gs_streamingpad_push_buffer(GsStreamingPad *spad, GstBuffer *_buffer);
GstBuffer*  gs_streamingpad_pop_buffer(GsStreamingPad* spad);
GstBuffer*  gs_streamingpad_peek_head_buffer(GsStreamingPad* spad);
GstBuffer*  gs_streamingpad_pop_buffer_unlock(GsStreamingPad* spad);
void        gs_streamingpad_set_max_cache_size(GsStreamingPad* spad, int size);
void        gs_streamingpad_set_cache_warning(GsStreamingPad* spad, gboolean on);
void        gs_streamingpad_set_delay_size(GsStreamingPad* spad, gint64 size);
gint64      gs_streamingpad_get_delay_size(GsStreamingPad* spad);
void   		gs_streamingpad_set_spring_delay_size(GsStreamingPad* spad, gint64 size);
void        gs_streamingpad_set_delay_time(GsStreamingPad* spad, GstClockTime delay);

int         gs_streamingpad_get_valid_size(GsStreamingPad* spad);
gboolean    gs_streamingpad_in_is_living(GsStreamingPad* spad);
gboolean    gs_streamingpad_out_is_living(GsStreamingPad* spad);
gboolean    gs_streamingpad_set_caps(GsStreamingPad* spad, GstCaps *caps);

#endif
